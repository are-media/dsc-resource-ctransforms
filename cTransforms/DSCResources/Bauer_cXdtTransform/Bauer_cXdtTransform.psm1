function Get-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Collections.Hashtable])]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$Target,

		[parameter(Mandatory = $true)]
		[System.String[]]
		$Transforms,

		[parameter(Mandatory = $true)]
		[System.String]
		$Destination
	)
	
	$returnValue = @{
		Target = $Target
		Transforms = $Transforms
		Destination = $Destination
	}

	$returnValue
}



function Set-TargetResource
{
	[CmdletBinding()]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$Target,

		[parameter(Mandatory = $true)]
		[System.String[]]
		$Transforms,

		[parameter(Mandatory = $true)]
		[System.String]
		$Destination
	)

    Write-Verbose "Doing Transform"

    $tempFolder = Join-Path $env:TEMP "cXdtTransforms";
    $tempOutputFile = Join-Path $tempFolder "transform.xml";
        
    $slowCheetahExe = [System.Environment]::GetEnvironmentVariable("tools-slowcheetah-exe","Machine");
        
    Write-Debug "temp Output File: $tempOutputFile"
    Write-Debug "slow Cheetah Exe: $slowCheetahExe"

    if (-Not (Test-Path $tempFolder))
    {
        Write-Debug "Creating temp folder $tempfolder"
        New-Item -Path $tempFolder -ItemType directory
    }
    
    Write-Debug "Copying target ($Target) to transform file ($tempOutputFile)"
    Copy-Item -LiteralPath $Target -Destination $tempOutputFile -Force

    foreach ($transform in $Transforms)
    {
        try
        {
            $startInfo = New-Object System.Diagnostics.ProcessStartInfo
            $startInfo.UseShellExecute = $false #Necessary for I/O redirection and just generally a good idea
            $startInfo.FileName = $slowCheetahExe
            $startInfo.Arguments = @($tempOutputFile, $transform, $tempOutputFile) -join " "            
                        
            $process = New-Object System.Diagnostics.Process
            $process.StartInfo = $startInfo
            
            Write-Verbose "Running transform $transform"    
            Write-Debug ("Starting {0} with {1}" -f $startInfo.FileName, $startInfo.Arguments)
            $process.Start() | Out-Null
            
            $process.WaitForExit()

            if($process)
            {
                $exitCode = $process.ExitCode
            }
            
            if ($exitCode -ne 0)
            {
                Throw-TerminatingError ($LocalizedData.UnexpectedReturnCode -f $exitCode.ToString())
            }
        }
        catch
        {
            Throw-TerminatingError ($LocalizedData.CouldNotStartProcess -f $Path) $_
        }
           
    }
        
    Write-Debug "Copying transform file ($tempOutputFile) to destination ($Destination)"
    Copy-Item -LiteralPath $tempOutputFile -Destination $Destination -Force

}



function Test-TargetResource
{
	[CmdletBinding()]
	[OutputType([System.Boolean])]
	param
	(
		[parameter(Mandatory = $true)]
		[System.String]
		$Target,

		[parameter(Mandatory = $true)]
		[System.String[]]
		$Transforms,

		[parameter(Mandatory = $true)]
		[System.String]
		$Destination
	)

    $slowCheetahExe = [System.Environment]::GetEnvironmentVariable("tools-slowcheetah-exe","Machine");

    if ([string]::IsNullOrWhiteSpace($slowCheetahExe))
    {
        Write-Error "Environment variable tools-slowcheetah-exe was not found or is empty"
        return $true;
    }

    if (-Not (Test-Path $slowCheetahExe))
    {
        Write-Error "Slow Cheeta Exe not found at $slowCheetahExe"        
        return $true;
    }
    
    if (-Not (Test-Path $Target))
    {
        Write-Error "Target file was not found at $Target"        
        return $true;
    }

    foreach ($transform in $Transforms)
    {
        if (-Not (Test-Path $transform))
        {
            Write-Error "transform file was not found at $transform"        
            return $true;
        }
    }

	$false

}

function Throw-TerminatingError
{
    param(
        [string] $Message,
        [System.Management.Automation.ErrorRecord] $ErrorRecord
    )

    $exception = new-object "System.InvalidOperationException" $Message,$ErrorRecord.Exception
    $errorRecord = New-Object System.Management.Automation.ErrorRecord $exception,"MachineStateIncorrect","InvalidOperation",$null
    throw $errorRecord
}

Export-ModuleMember -Function *-TargetResource
