﻿$moduleName = "cTransforms"
$powershellPath = "C:\Program Files\WindowsPowerShell\Modules"
$modulePath = Join-Path $powershellPath $moduleName
$moduleDscPath = Join-Path $modulePath "DSCResources"


#### Setup cXdtTransform #####
$resourceFriendlyName = "cXdtTransform"
$resourceName = "Bauer_$resourceFriendlyName"
$resoucePath = Join-Path $moduleDscPath $resourceName

$target = New-xDscResourceProperty -Name "Target" -Type String -Attribute Required -Description "Base XML file to apply transforms to"

$transforms = New-xDscResourceProperty -Name "Transforms" -Type String[] -Attribute Required -Description "List of transform files to apply"

$destination = New-xDscResourceProperty -Name "Destination" -Type String -Attribute Key -Description "Where the final result should be outputted to"

$properties = @($target, $transforms,$destination)

Write-Host "resource path is $resoucePath"

if (Test-Path $resoucePath)
{
    Write-Host "Updating resource $resourceName"
    Update-xDscResource -Name $resourceName -Property $properties -Force
}
else
{
    Write-Host "Creating new resource $resourceName"
    New-xDscResource -Name $resourceName -Property $properties -Path $powershellPath -ModuleName $moduleName -FriendlyName $resourceFriendlyName -Force
}